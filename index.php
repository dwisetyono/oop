<?php

    require("animal.php");
    require("frog.php");
    require("ape.php");

    $shaun = new animal("shaun");

    echo "Nama Hewan:  $shaun->name <br>";
    echo "Jumlah Kaki: $shaun->legs <br>";
    echo "Cool blooded: $shaun->cool_blooded <br><br>";

    $object = new frog("buduk");

    echo "Nama Hewan:  $object->name <br>";
    echo "Jumlah Kaki:  $object->legs <br>";
    echo "Cool blooded:  $object->cool_blooded <br>";
    $object->jump();

    $object2 = new ape("kera sakti");

    echo "Nama Hewan:  $object2->name <br>";
    echo "Jumlah Kaki:  $object2->legs <br>";
    echo "Cool blooded:  $object2->cool_blooded <br>";
    $object2->yell();

?>